package com.mu.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mu.dao.ProductDAOImpl;
import com.mu.entity.Employee;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductDAOImpl productDAOImpl;
	
	public List<Employee> findAll() {
		// TODO Auto-generated method stub
		return productDAOImpl.findAll();
	}

}
