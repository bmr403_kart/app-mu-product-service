package com.mu.dao; 

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import com.mu.entity.Employee;

public interface ProductDAO extends CassandraRepository<Employee, Integer> {
	
	@Query("select * from emp")
	List<Employee> findAll();
}
