package com.mu.dao;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import com.mu.entity.Employee;

public class ProductDAOImpl implements ProductDAO {

	Logger log = LogManager.getLogger(ProductDAOImpl.class);
	
	@Autowired
	private ProductDAO productDAO;

	public <S extends Employee> List<S> saveAll(Iterable<S> entites) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Employee> findAllById(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	public Slice<Employee> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	public <S extends Employee> S insert(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	public <S extends Employee> List<S> insert(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	public <S extends Employee> S save(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	public Optional<Employee> findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean existsById(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		
	}

	public void delete(Employee entity) {
		// TODO Auto-generated method stub
		
	}

	public void deleteAll(Iterable<? extends Employee> entities) {
		// TODO Auto-generated method stub
		
	}

	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	public List<Employee> findAll() {
		// TODO Auto-generated method stub
		return productDAO.findAll();
	}
	
}
